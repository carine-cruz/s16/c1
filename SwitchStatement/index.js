
function changeSize(){

	let size = document.getElementById('inputSize').value;
	let target = document.getElementById('targetP');

	size = size.toUpperCase();	

	console.log(`button invoked`);
	console.log(`size: ${size}`);
	console.log(`element: ${target}`);

	switch(size){
		case `S`: target.style.cssText = `font-size: 8px`; 
						break;
		case `M`: target.style.cssText = `font-size: 32px`; 
						break;
		case `L`: target.style.cssText = `font-size: 64px`; 
						break;
		default : target.style.cssText = `font-size: 16px`; 
	}
}
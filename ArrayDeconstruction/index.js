let studentNames = [`Harry Potter`,`Ron Weasley`,`Hermione Granger`,`Draco Malfoy`,`Lavender Brown`,`Parvati Patil`];

let [student1, student2, student3, student4, student5,student6,student7=`No one`] = studentNames;

console.log(`${student1} is taking up Quidditch lesosns.`);
console.log(`${student2} lost 20 points for their house.`);
console.log(`${student3} is taking up Divination exams.`);
console.log(`${student4} received a howler this morning.`);
console.log(`${student5} is banned from Hogsmeade.`);
console.log(`${student6} is assisting Professor Flitwick.`);
console.log(`${student7}'s favorite food is pumpkin pie.`);
/*
Create a function using ES6 Arrow function that accepts an object, and inside the function, use object destructuring to create distinct variables for each object property. Return a string of a complete sentence bearing the values from the object destruction method. Use template literals in this activity.
*/

let fruit = { type: `apple`,
			  variety: `fiji`,
			  taste: `sweet`,
			  color: `red` 
}

/*function deconstructFruit(obj){
	let {type, variety, taste, color} = obj;
	let strSentence = `The ${variety} ${type} which is color ${color}, tastes ${taste}. `
	return strSentence;
} 

console.log(deconstructFruit(fruit));*/

//variable/functionName = (argument) => {<return result of statement>;}

let deconstructFruit2 = (obj) => {
								  let {type, variety, taste, color} = obj;
								  return `The ${variety} ${type} which is color ${color}, tastes ${taste}. `}

console.log(deconstructFruit2(fruit));

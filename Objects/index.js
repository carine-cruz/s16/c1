let person = {
	firstName: `Lisa`,
	lastName : `Montero`,
	age : 25,
	address : { 
				city: `Makati City`,
				country: `Philippines`,	
			},
	reference: [{
					name: `John Sanchez`,
					emailaddress: `johns@email.com`
				},
				{
					name: `Samantha Jones`,
					emailaddress: `sammy@email.com`
				},
				{
					name: `Jessica Samson`,
					emailaddress: `jsamson@test.com`
				}
				],
	friends: [`Angela`,`Keith`,`Steph`],
	isAdmin : true,
	introduction : function(){
						console.log(`Hi! My name is ${this.firstName} ${this.lastName}. I am ${this.age} years old.`);
					}			
}

person.introduction();
let students = [`student1`, `student2`, `student3`, `student4`, `student5`, `student5`];

console.log(students);
//get array length
console.log(`Array length:`, students.length);
//add new element at the BEGINNING of array
students.unshift(`studentA`);
console.log(`Add new element (start):`,students);
//add new element at the END of array
students.push(`student7`);
console.log(`Add new element (end):`,students);
//remove element at the BEGINNING of array
students.shift();
console.log(`Remove first element:`, students);
//remove element at the END of array
students.pop();
console.log(`Remove last element:`, students);
//find index of item
console.log(`index of student4:`, students.indexOf(`student4`));
//find index of first duplicate item
console.log(`index of first item of duplicate:`,students.indexOf(`student5`));

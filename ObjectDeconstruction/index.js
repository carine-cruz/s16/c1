let person = {
				name: {
						firstName: `Artemis`,
						lastName: `Cross`
						},
				jobTitle: `architect`,
				city: `Quezon City` 	
};

let {name:{firstName,lastName},jobTitle,city}=person;

console.log(`${firstName} ${lastName} is an ${jobTitle} who works in ${city}.`);
let objIntroduction = {
	fName : prompt(`Enter first name:`),
	lName : prompt(`Enter last name:`),
	Age : parseInt(prompt(`Enter age:`)),
	job : prompt(`Enter current job:`),
	reason : prompt(`Enter reason for career shift:`),
	goals : prompt(`Enter goals`)
}




function printIntro(info){
	let strIntro = `Hi! I am ${info.fName} ${info.lName}, ${info.Age} years of age. I currently work as a ${info.job}. I am shifting careers due to ${info.reason}. I want to ${info.goals}.`;

	return strIntro;
}

console.log(printIntro(objIntroduction));